<?php

namespace App;

use Jenssegers\Date\Date;
use Illuminate\Database\Eloquent\Model;

class Cuadre extends Model
{
  protected $fillable = [
    'cuadre_venta',
    'cuadre_base',
    'cuadre_propina',
    'cuadre_descripcion'
  ];

  // Getters

  public function getCreatedAtAttribute($date)
  {
    return new Date($date);
  }

  // Setters



  // Relationships

  public function pagos()
  {
    return $this->hasMany(Pago::class);
  }

  public function datafonos()
  {
    return $this->hasMany(Datafono::class);
  }

  // Methods

  public function crearPagos($pagos)
  {
    for ($i=0; $i < count($pagos); $i++) {
      $pagosSeparados[] = explode('-',$pagos[$i]);
      $pago = new Pago;
      $pago->pago_nombre = $pagosSeparados[$i][0];
      $pago->pago_valor = $pagosSeparados[$i][1];
      if (count($pagosSeparados[$i]) > 2) {
        $pago->pago_desc = $pagosSeparados[$i][2];
      }
      $pago->cuadre_id = $this->id;
      $pago->save();
    }
  }

  public function crearDatafonos($datafonos)
  {
    for ($i=0; $i < count($datafonos); $i++) {
      $datafonosSeparados[] = explode('-',$datafonos[$i]);
      $datafono = new Datafono;
      $datafono->datafono_tipo = $datafonosSeparados[$i][0];
      $datafono->datafono_valor = $datafonosSeparados[$i][1];
      if (count($datafonosSeparados[$i]) > 2) {
        $datafono->datafono_desc = $datafonosSeparados[$i][2];
      }
      $datafono->cuadre_id = $this->id;
      $datafono->save();
    }
  }

  public function editarPagos($pagos)
  {
    $this->pagos()->delete();
    for ($i=0; $i < count($pagos); $i++) {
      $pagosSeparados[] = explode('-',$pagos[$i]);
      $pago = new Pago;
      $pago->pago_nombre = $pagosSeparados[$i][0];
      $pago->pago_valor = $pagosSeparados[$i][1];
      if (count($pagosSeparados[$i]) > 2) {
        $pago->pago_desc = $pagosSeparados[$i][2];
      }
      $pago->cuadre_id = $this->id;
      $pago->save();
    }

  }

  public function editarDatafonos($datafonos)
  {
    $this->datafonos()->delete();
    for ($i=0; $i < count($datafonos); $i++) {
      $datafonosSeparados[] = explode('-',$datafonos[$i]);
      $datafono = new Datafono;
      $datafono->datafono_tipo = $datafonosSeparados[$i][0];
      $datafono->datafono_valor = $datafonosSeparados[$i][1];
      if (count($datafonosSeparados[$i]) > 2) {
        $datafono->datafono_desc = $datafonosSeparados[$i][2];
      }
      $datafono->cuadre_id = $this->id;
      $datafono->save();
    }
  }

  public function totalBase()
  {
    $total = $this->cuadre_venta + $this->cuadre_base + $this->cuadre_propina;

    return $total;
  }

  public function sumaPagos()
  {
    $sumaPagos = 0;
    foreach ($this->pagos as $key => $pago) {
      $sumaPagos += $pago->pago_valor;
    }

    return $sumaPagos;
  }

  public function sumaDatafonos()
  {
    $sumaDatafonos = 0;
    foreach ($this->datafonos as $key => $datafono) {
      $sumaDatafonos += $datafono->datafono_valor;
    }

    return $sumaDatafonos;
  }

  public function totalSumaPD()
  {
    $totalSumaPD = $this->sumaPagos() + $this->sumaDatafonos();

    return $totalSumaPD;
  }

  public function totalFinal()
  {
    $totalFinal = $this->totalBase() - $this->totalSumaPD();

    return $totalFinal;
  }

}
