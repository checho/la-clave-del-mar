<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
  protected $fillable = [
    'pago_nombre',
    'pago_valor',
    'pago_desc',
    'cuadre_id'
  ];

  public function cuadre()
  {
    return $this->belongsTo(Cuadre::class);
  }
}
