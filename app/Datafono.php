<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datafono extends Model
{
  protected $fillable = [
    'datafono_tipo',
    'datafono_valor',
    'datafono_desc',
    'cuadre_id'
  ];

  public function cuadre()
  {
    return $this->belongsTo(Cuadre::class);
  }
}
