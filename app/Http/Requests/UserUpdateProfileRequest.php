<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
          'name' => [
            'required',
            'string',
            'max:60'
          ],
          'email' => [
            'required',
            'string',
            'email',
            'max:200',
            Rule::unique('users')->ignore($this->route('user')->id)
          ],
        ];

        return $rules;
    }
}
