<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DatafonoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cuadre_id' => 'required',
            'datafono_tipo' => 'required|string|alpha_num|max:50',
            'datafono_valor' => 'required|integer',
        ];
    }
}
