<?php

namespace App\Http\Requests;

use App\Rules\ValidFormatPagos;
use Illuminate\Foundation\Http\FormRequest;

class CuadreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cuadre_venta' => 'required|integer|min:0|max:10000000',
            'cuadre_base' => 'required|integer|min:0|max:1000000',
            'cuadre_propina' => 'required|integer|min:0|max:500000',

            // 'pagos' => 'required',
            // 'datafonos' => 'required',
        ];
    }
}
