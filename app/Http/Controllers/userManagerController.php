<?php

namespace App\Http\Controllers;

use App\User;
use Hash;
use Auth;
use App\Http\Requests\UserUpdatePasswordRequest;
use App\Http\Requests\UserUpdateProfileRequest;
use Illuminate\Http\Request;

class userManagerController extends Controller
{
  public function profile()
  {
    return view('admin.userManager.profile');
  }

  public function showEditProfile()
  {
    $user = Auth::user();
    return view('admin.userManager.editProfile', compact('user'));
  }

  public function editProfile(UserUpdateProfileRequest $request, User $user)
  {

    if($request->hasFile('image')){
      $user->image = $request->file('image')->store('users/avatar');
    }

    $user->update($request->except('image'));
    return redirect()->route('profile')->with('info', 'Tus datos fueron actualizados con éxito');
  }

  public function showEditPassword()
  {
    return view('admin.userManager.editPassword');
  }

  public function editPassword(UserUpdatePasswordRequest $request, User $user)
  {
    if (!(Hash::check($request->current_password, $user->password))) {
      return redirect()->back()->with('error','Tú contraseña no coincide con la actual');
    }
    if (strcmp($request->current_password, $request->password) == 0) {
      return redirect()->back()->with('error','La nueva contraseña debe ser difente a la antigua, Por favor elija una nueva contraseña.');
    }
    $user->password = bcrypt($request->password);
    $user->save();
    return redirect()->route('profile')->with('info', 'tu clave fue actualizada');
  }
}
