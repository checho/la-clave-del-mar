<?php

namespace App\Http\Controllers;

use App\Cuadre;
use App\Pago;
use App\Datafono;
use App\Http\Requests\CuadreRequest;
use Illuminate\Http\Request;

class CuadreController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $cuadres = Cuadre::all();
    return view('admin.Cuadres.index', compact('cuadres'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view('admin.Cuadres.create');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(CuadreRequest $request)
  {
    $cuadre = Cuadre::create($request->all());

    $cuadre->crearPagos($request->pagos);
    $cuadre->crearDatafonos($request->datafonos);

    return redirect()->route('cuadres.index')->with('info', 'Se ha creado el cuadre con éxito');
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Cuadre  $cuadre
  * @return \Illuminate\Http\Response
  */
  public function show(Cuadre $cuadre)
  {
    return view('admin.Cuadres.show', compact('cuadre'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Cuadre  $cuadre
  * @return \Illuminate\Http\Response
  */
  public function edit(Cuadre $cuadre)
  {
    return view('admin.Cuadres.edit', compact('cuadre'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Cuadre  $cuadre
  * @return \Illuminate\Http\Response
  */
  public function update(CuadreRequest $request, Cuadre $cuadre)
  {
    $cuadre->update($request->all());

    $cuadre->editarPagos($request->pagos);
    $cuadre->editarDatafonos($request->datafonos);

    return redirect()->route('cuadres.index')->with('info', 'Se ha editado el cuadre con éxito');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Cuadre  $cuadre
  * @return \Illuminate\Http\Response
  */
  public function destroy(Cuadre $cuadre)
  {
    $cuadre->pagos()->delete();
    $cuadre->datafonos()->delete();

    $cuadre->delete();

    return back()->with('info', 'Se ha eliminado el cuadre con éxito');
  }
}
