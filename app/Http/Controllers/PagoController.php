<?php

namespace App\Http\Controllers;

use App\Pago;
use App\Cuadre;
use App\Http\Requests\PagoRequest;
use App\Http\Requests\PagoUpdateRequest;
use Illuminate\Http\Request;

class PagoController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $pagos = Pago::all();
    return view('admin.Pagos.index', compact('pagos'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $cuadres = Cuadre::all();
    return view('admin.Pagos.create', compact('cuadres'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(PagoRequest $request)
  {
    $pagos = $request->pagos;
    for ($i=0; $i < count($pagos); $i++) {
      $pagosSeparados[] = explode('-',$pagos[$i]);
      $pago = new Pago;
      $pago->pago_nombre = $pagosSeparados[$i][0];
      $pago->pago_valor = $pagosSeparados[$i][1];
      $pago->pago_desc = $request->pago_desc;
      $pago->cuadre_id = $request->cuadre_id;
      $pago->save();
    }
    return redirect()->route('pagos.index')->with('info', 'Se han creado los pagos con éxito');
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Pago  $pago
  * @return \Illuminate\Http\Response
  */
  public function show(Pago $pago)
  {
    return view('admin.Pagos.show', compact('pago'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Pago  $pago
  * @return \Illuminate\Http\Response
  */
  public function edit(Pago $pago)
  {
    $cuadres = Cuadre::all();
    return view('admin.Pagos.edit', compact('pago', 'cuadres'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Pago  $pago
  * @return \Illuminate\Http\Response
  */
  public function update(PagoUpdateRequest $request, Pago $pago)
  {
    $pago->update($request->all());
    return redirect()->route('pagos.index')->with('info', 'Se ha actualizado el pago con éxito');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Pago  $pago
  * @return \Illuminate\Http\Response
  */
  public function destroy(Pago $pago)
  {
    $pago->delete();

    return back()->with('info', 'Se ha eliminado el pago con éxito');
  }
}
