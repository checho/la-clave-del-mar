<?php

namespace App\Http\Controllers;

use App\Datafono;
use App\Cuadre;
use App\Http\Requests\DatafonoRequest;
use App\Http\Requests\DatafonoUpdateRequest;
use Illuminate\Http\Request;

class DatafonoController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $datafonos = Datafono::all();
    return view('admin.Datafonos.index', compact('datafonos'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $cuadres = Cuadre::all();
    return view('admin.Datafonos.create', compact('cuadres'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(DatafonoRequest $request)
  {
    $datafonos = $request->datafonos;
    for ($i=0; $i < count($datafonos); $i++) {
      $datafonosSeparados[] = explode('-',$datafonos[$i]);
      $datafono = new Datafono;
      $datafono->datafono_tipo = $datafonosSeparados[$i][0];
      $datafono->datafono_valor = $datafonosSeparados[$i][1];
      $datafono->datafono_desc = $request->datafono_desc;
      $datafono->cuadre_id = $request->cuadre_id;
      $datafono->save();
    }
    return redirect()->route('datafonos.index')->with('info', 'Se han creado los datáfonos con éxito');
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Datafono  $datafono
  * @return \Illuminate\Http\Response
  */
  public function show(Datafono $datafono)
  {
    return view('admin.Datafonos.show', compact('datafono'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Datafono  $datafono
  * @return \Illuminate\Http\Response
  */
  public function edit(Datafono $datafono)
  {
    $cuadres = Cuadre::all();
    return view('admin.Datafonos.edit', compact('datafono', 'cuadres'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Datafono  $datafono
  * @return \Illuminate\Http\Response
  */
  public function update(DatafonoUpdateRequest $request, Datafono $datafono)
  {
    $datafono->update($request->all());
    return redirect()->route('datafonos.index')->with('info', 'Se ha actualizado el datáfono con éxito');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Datafono  $datafono
  * @return \Illuminate\Http\Response
  */
  public function destroy(Datafono $datafono)
  {
    $datafono->delete();

    return back()->with('info', 'Se ha eliminado el datáfono con éxito');
  }
}
