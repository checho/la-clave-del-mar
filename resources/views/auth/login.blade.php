@extends('auth.layout')

@section('content')
<div class="login-box-body">
  <p class="login-box-msg">Logueate para iniciar sesión</p>
  <form class="form-login mt-5" method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}
    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
      <label for="email" class="sr-only">Correo electrónico</label>
      <div>
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Correo electrónico" required autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
        <label class="control-label" for="email"><i class="fa fa-times-circle-o"></i>
          <strong>{{ $errors->first('email') }}</strong>
        </label>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
      <label for="password" class="sr-only">Contraseña</label>
      <div>
        <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has('password'))
        <span style="color:#F78181" class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
      </div>
    </div>
    <div class="row">
      <div class="col-xs-8">
        <div class="checkbox icheck">
          <label>
            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
          </label>
        </div>
      </div>
      <!-- /.col -->
      <div class="col-xs-4">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
      </div>
      <!-- /.col -->
    </div>
  </form>
  {{-- <div class="social-auth-links text-center">
    <p>- OR -</p>
    <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
      Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
      </div> --}}
      <!-- /.social-auth-links -->
      <a class="btn btn-link" href="{{ route('password.request') }}">
        Olvisdaste tu contraseña?
      </a>
      <center>
        <p class="text-muted">&copy; 2019</p>
      </center>
    </div>
@stop
