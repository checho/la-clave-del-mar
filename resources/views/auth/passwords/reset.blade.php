@extends('auth.layout')

@section('content')
<div class="login-box-body">
  <p class="login-box-msg">Cambiar contraseña</p>
  <form class="form-login mt-5" method="POST" action="{{ route('password.request') }}">
    {{ csrf_field() }}
    <input type="hidden" name="token" value="{{ $token }}">
    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
      <label for="email" class="sr-only">E-Mail Address</label>
      <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" placeholder="Correo electrónico" required autofocus>
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      @if ($errors->has('email'))
      <span class="help-block">
        <strong>{{ $errors->first('email') }}</strong>
      </span>
      @endif
    </div>
    <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
      <label for="password" class="sr-only">Contraseña</label>
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required>
      @if ($errors->has('password'))
      <span class="help-block">
        <strong>{{ $errors->first('password') }}</strong>
      </span>
      @endif
    </div>
    <div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
      <label for="password-confirm" class="sr-only">Confirmar contraseña</label>
      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Repita su contraseña" required>
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      @if ($errors->has('password_confirmation'))
      <span class="help-block">
        <strong>{{ $errors->first('password_confirmation') }}</strong>
      </span>
      @endif
    </div>
    <button type="submit" class="btn btn-primary btn-block btn-flat">
      Cambiar contraseña
    </button>
  </form>
</div>
<!-- /.login-box-body -->
@endsection
