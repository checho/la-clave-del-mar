@extends('auth.layout')

@section('content')
<div class="login-box-body">
  <p class="login-box-msg">Recuperar contraseña</p>
  @if (session('status'))
  <div class="alert alert-success">
    {{ session('status') }}
  </div>
  @endif
  <form class="form-login mt-5" method="POST" action="{{ route('password.email') }}">
    {{ csrf_field() }}
    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
      <label for="email" class="sr-only">Correo electrónico</label>
      <div>
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Correo electrónico" required autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
        <label class="control-label" for="email"><i class="fa fa-times-circle-o"></i>
          <strong>{{ $errors->first('email') }}</strong>
        </label>
        @endif
      </div>
    </div>
    <button type="submit" class="btn btn-primary">
      Enviar correo de confirmación
    </button>
    <a class="btn btn-info pull-right" href="{{ URL::previous() }}">Volver</a>
  </form>
</div>
<!-- /.login-box-body -->
@endsection
