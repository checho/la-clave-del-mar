<ul class="sidebar-menu" data-widget="tree">
  <li class="header">Navegación</li>
  <!-- Optionally, you can add icons to the links -->
  <li {{ request()->is('administrador') ? 'class=active' : '' }}><a href="{{ url('/administrador') }}"><i class="fa fa-dashboard"></i> <span>Inicio</span></a></li>
  <li {{ request()->is('admin/users') ? 'class=active' : '' }}><a href="{{ route('users.index') }}"><i class="fa fa-user"></i> <span>Usuarios</span></a></li>
  <!-- <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li> -->
  <li class="treeview {{ request()->is(['admin/cuadres', 'admin/pagos', 'admin/datafonos']) ? 'active' : '' }} ">
    <a href="#"><i class="fa fa-bars"></i> <span>Todos los días</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li {{ request()->is('admin/cuadres') ? 'class=active' : '' }}><a href="{{ route('cuadres.index') }}"><i class="fa fa-usd"></i>Cuadres</a></li>
      <li {{ request()->is('admin/pagos') ? 'class=active' : '' }}><a href="{{ route('pagos.index') }}"><i class="fa fa-money"></i>Pagos</a></li>
      <li {{ request()->is('admin/datafonos') ? 'class=active' : '' }}><a href="{{ route('datafonos.index') }}"><i class="fa fa-credit-card"></i>Datafonos</a></li>
    </ul>
  </li>
</ul>
