<div class="modal modal-warning fade" id="deleteDatafono-{{ $datafono->id }}">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">¿Deseas eliminar este datáfono?</h4>
        </div>
        <div class="modal-body">
          <strong>Recuerda que pueden haber cuadres asociados a este datáfono, ¿Estas seguro de querer eliminarlo?</strong>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
          <form action="{{ route('datafonos.destroy', $datafono) }}" method="post">
            {{ csrf_field() }} {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-outline btn-danger">Si, eliminar</button>
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
