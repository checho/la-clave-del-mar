{{ csrf_field() }} {{ method_field('PUT') }}
<div class="row">
  <div class="form-group col-md-2">
    <label for="cuadre_fecha">Fecha del datáfono</label>
    <input class="form-control" type="text" value="{{ $datafono->created_at }}" disabled>
  </div>
  <div class="form-group col-md-6 {{ $errors->has('cuadre_id') ? 'has-error' : '' }}">
    <label for="cuadre_id">Seleccione el cuadre <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="Seleccione la fecha del cuadre a la cual quiere añadir el pago"></i></label>
    <select class="form-control select2-cuadre" name="cuadre_id" data-placeholder="Seleccione el cuadre" style="width: 100%;">
      @foreach($cuadres as $cuadre)
        <option {{ $cuadre->id === $datafono->cuadre_id ? 'selected' : '' }} value="{{ $cuadre->id }}">{{ $cuadre->created_at->toFormattedDateString() }}</option>
      @endforeach
    </select>
    @if ($errors->has('cuadre_id'))
    <label class="control-label" for="cuadre_id"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('cuadre_id') }}</strong>
    </label>
    @endif
  </div>
</div>
<div class="row">
  <div class="form-group col-md-6 {{ $errors->has('datafono_tipo') ? 'has-error' : '' }}">
    <label for="datafono_tipo">Datáfono</label>
    <select class="form-control select2-datafono-tipo" name="datafono_tipo" data-placeholder="Nombre del datáfono" style="width: 100%;">
      <option selected value="{{ $datafono->datafono_tipo }}">{{ $datafono->datafono_tipo }}</option>
    </select>
    @if ($errors->has('datafono_tipo'))
    <label class="control-label" for="datafono_tipo"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('datafono_tipo') }}</strong>
    </label>
    @endif
  </div>
  <div class="form-group col-md-6 {{ $errors->has('datafono_valor') ? 'has-error' : '' }}">
    <label for="datafono_valor">Valor datáfono</label>
    <select class="form-control select2-datafono-valor" name="datafono_valor" data-placeholder="Valor del datáfono" style="width: 100%;">
      <option selected value="{{ $datafono->datafono_valor }}">{{ $datafono->datafono_valor }}</option>
    </select>
    @if ($errors->has('datafono_valor'))
    <label class="control-label" for="datafono_valor"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('datafono_valor') }}</strong>
    </label>
    @endif
  </div>
</div>
<div class="row">
  <div class="form-group col-md-12">
    <label for="datafono_desc">Descripción del datáfono</label>
    <textarea id="editor-datafono" name="datafono_desc" rows="10" cols="80">
      {{ old('datafono_desc', $datafono->datafono_desc) }}
    </textarea>
  </div>
</div>
<button type="submit" class="btn btn-success">Terminar</button>
<a href="{{ URL::previous() }}" class="btn btn-info pull-right"><i class="fa fa-chevron-left"></i> Regresar</a>
