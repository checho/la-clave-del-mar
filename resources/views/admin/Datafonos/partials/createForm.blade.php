{{ csrf_field() }}
<div class="row">
  <div class="form-group col-md-2">
    <label for="cuadre_fecha">Fecha del datáfono</label>
    <input class="form-control" type="text" value="{{ Carbon\Carbon::now() }}" disabled>
  </div>
  <div class="form-group col-md-6 {{ $errors->has('cuadre_id') ? 'has-error' : '' }}">
    <label for="cuadre_id">Seleccione el cuadre <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="Seleccione la fecha del cuadre a la cual quiere añadir el pago"></i></label>
    <select class="form-control select2-cuadre" name="cuadre_id" data-placeholder="Seleccione el cuadre" style="width: 100%;">
      <option selected value=""></option>
      @foreach($cuadres as $cuadre)
      <option value="{{ $cuadre->id }}">{{ $cuadre->created_at->toFormattedDateString() }}</option>
      @endforeach
    </select>
    @if ($errors->has('cuadre_id'))
    <label class="control-label" for="cuadre_id"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('cuadre_id') }}</strong>
    </label>
    @endif
  </div>
</div>
<div class="row">
  <div class="form-group col-md-6 {{ $errors->has('datafonos') ? 'has-error' : '' }}">
    <label for="datafonos">Datáfono / Valor datáfono <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="Ej: visa-50000 AMEX-78500"></i></label>
    <select class="form-control select2-datafonos" multiple="multiple" name="datafonos[]" data-placeholder="Escriba el tipo de datáfono y su valor, separelos por '-'" style="width: 100%;">
    </select>
    @if ($errors->has('datafonos'))
    <label class="control-label" for="datafonos"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('datafonos') }}</strong>
    </label>
    @endif
  </div>
</div>
<div class="row">
  <div class="form-group col-md-12">
    <label for="datafono_desc">Descripción del datáfono</label>
    <textarea id="editor-datafono" name="datafono_desc" rows="10" cols="80">
      {{ old('datafono_desc') }}
    </textarea>
  </div>
</div>
<button type="submit" class="btn btn-success">Terminar</button>
<a href="{{ URL::previous() }}" class="btn btn-info pull-right"><i class="fa fa-chevron-left"></i> Regresar</a>
