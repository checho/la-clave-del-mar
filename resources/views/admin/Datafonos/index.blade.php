@extends('admin.layout')

@section('styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop

@section('header')
  <h1>
    Datáfonos
    <small>Lista de todos los datáfonos</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/administrador') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Datáfonos</li>
  </ol>
@stop

@section('content')
  <div class="box box-primary">
    <div class="box-header with-border">
      @include('admin.messages.info')
      <h3 class="box-title">Lista de datáfonos</h3>
      <a href="{{ route('datafonos.create') }}" class="btn btn-primary pull-right">Generar nuevo datáfono <i class="fa fa-plus"></i></a>
    </div>
    <div class="box-body">
      <table id="datafonos-table" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th width="20px">ID</th>
            <th>Tipo de datáfono</th>
            <th>Valor del datáfono</th>
            <th>descripción del datáfono</th>
            <th>Fecha del datáfono</th>
            <th>Cuadre al que pertenece</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach($datafonos as $datafono)
            <tr>
              <td>{{ $datafono->id }}</td>
              <td>{{ $datafono->datafono_tipo }}</td>
              <td>{{ $datafono->datafono_valor }}</td>
              @if(isset($datafono->datafono_desc))
                <td>{!! str_limit($datafono->datafono_desc, 30, '...') !!}</td>
              @else
                <td>No hay descripción</td>
              @endif
              <td>{{ $datafono->created_at }}</td>
              <td><a href="{{ route('cuadres.show', $datafono->cuadre->id) }}">{{ $datafono->cuadre->created_at->toFormattedDateString() }}</a></td>

              <td class="pull-right">
                <a class="btn btn-xs btn-info" href="{{ route('datafonos.show', $datafono) }}" ><i class="fa fa-eye"></i></a>
                <a class="btn btn-xs btn-warning" href="{{ route('datafonos.edit', $datafono) }}" ><i class="fa fa-pencil-square-o"></i></a>
                <a class="btn btn-xs btn-danger" href="#" data-toggle="modal" data-target="#deleteDatafono-{{ $datafono->id }}"><i class="fa fa-trash"></i></a>
    					</td>
            </tr>

            @include('admin.Datafonos.partials.deleteModal')

          @endforeach
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
@stop

@section('scripts')
  <!-- DataTables -->
  <script src="/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

  <!-- page script -->
  <script>
  $(function () {
    $('#datafonos-table').DataTable({
      'paging'      : true,
      'searching'   : true,
      'ordering'    : true,
      'autoWidth'   : false,
      "scrollX"     : true,
    })
  })
</script>
@stop
