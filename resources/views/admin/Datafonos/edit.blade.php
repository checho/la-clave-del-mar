@extends('admin.layout')

@section('styles')
  <!-- Select2 -->
  <link rel="stylesheet" href="/adminlte/bower_components/select2/dist/css/select2.min.css">
@stop

@section('header')
  <h1>
    Formulario de edición de datáfonos
    <small>formulario donde se editan datáfonos</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/administrador') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{ route('datafonos.index') }}"><i class="fa fa-money"></i> Datáfono</a></li>
    <li class="active">Editar datáfono</li>
  </ol>
@stop

@section('content')
  <div class="box box-success">
    <div class="box-header with-border">
      <h3 class="box-title">Editar datáfono</h3>
      <a href="{{ URL::previous() }}" class="btn btn-info pull-right"><i class="fa fa-chevron-left"></i> Regresar</a>
    </div>
    <div class="box-body">
      @include('admin.messages.error')      
      <form role="form" action="{{ route('datafonos.update',$datafono) }}" method="post">
        @include('admin.Datafonos.partials.editForm')
      </form>
    </div>
  </div>
@stop

@section('scripts')
  <!-- Select2 -->
  <script src="/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
  <!-- CK Editor -->
  <script src="/adminlte/bower_components/ckeditor/ckeditor.js"></script>

  <!-- Page script -->
  <script>
    $(function () {
      //Initialize Select2 Elements
      $('.select2-cuadre').select2({
        language: "es"
      })
      $('.select2-datafono-tipo').select2({
        tags: true,
        language: "es",
        allowClear: true
      })
      $('.select2-datafono-valor').select2({
        tags: true,
        language: "es",
        allowClear: true
      })
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor-datafono')
      //tooltips
      $('[data-toggle="tooltip"]').tooltip()
    })
  </script>

@stop
