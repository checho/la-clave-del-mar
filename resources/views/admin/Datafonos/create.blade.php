@extends('admin.layout')

@section('styles')
  <!-- Select2 -->
  <link rel="stylesheet" href="/adminlte/bower_components/select2/dist/css/select2.min.css">
@stop

@section('header')
  <h1>
    Formulario de datáfonos
    <small>formulario donde se crean datáfonos</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/administrador') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{ route('datafonos.index') }}"><i class="fa fa-credit-card"></i> Datáfonos</a></li>
    <li class="active">Crear datáfono</li>
  </ol>
@stop

@section('content')
  <div class="box box-success">
    <div class="box-header with-border">
      <h3 class="box-title">Generar datáfonos</h3>
      <a href="{{ URL::previous() }}" class="btn btn-info pull-right"><i class="fa fa-chevron-left"></i> Regresar</a>
    </div>
    <div class="box-body">
      @include('admin.messages.error')
      <form role="form" action="{{ route('datafonos.store') }}" method="post">
        @include('admin.Datafonos.partials.createForm')
      </form>
    </div>
  </div>
@stop

@section('scripts')
  <!-- Select2 -->
  <script src="/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
  <!-- CK Editor -->
  <script src="/adminlte/bower_components/ckeditor/ckeditor.js"></script>

  <!-- Page script -->
  <script>
    $(function () {
      //Initialize Select2 Elements
      $('.select2-cuadre').select2({
        language: "es",                
      })
      $('.select2-datafonos').select2({
        tags: true,
        language: "es"
      })
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor-datafono')
      //tooltips
      $('[data-toggle="tooltip"]').tooltip()
    })
  </script>

@stop
