@extends('admin.layout')

@section('styles')

@stop

@section('header')
  <h1>
    Detalle de datáfono
    <small>Detalle de un datáfono específico</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/administrador') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Datáfono - {{ $datafono->created_at->toFormattedDateString() }}</li>
  </ol>
@stop

@section('content')
  <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Detalle del datáfono - {{ $datafono->created_at->toFormattedDateString() }}</h3><span> Cuadre al que pertenece - <a href="{{ route('cuadres.show', $datafono->cuadre->id) }}">{{ $datafono->cuadre->created_at->toFormattedDateString() }}</a></span>
      <a href="{{ URL::previous() }}" class="btn btn-info pull-right"><i class="fa fa-chevron-left"></i> Regresar</a>
      <a style="margin-right: 10px"  href="{{ route('datafonos.edit', $datafono) }}" class="btn btn-primary pull-right"><i class="fa fa-pencil"></i> Editar datáfono</a>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <h3>Tipo de datáfono:</h3>
          <div class="col-md-11 offset-1">
            <strong>{{ $datafono->datafono_tipo }}</strong>
          </div>
        </div>
        <div class="col-md-6">
          <h3>Valor del datáfono:</h3>
          <div class="col-md-11 offset-1">
            <strong>{{ $datafono->datafono_valor }}</strong>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <h3>Descripción del datáfono</h3>
          <br>
          {!! $datafono->datafono_desc or 'No hay descripción' !!}
        </div>
      </div>
    </div>
  </div>
@stop

@section('scripts')

@stop
