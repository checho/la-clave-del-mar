@extends('admin.layout')

	@section('content')
		@if (session('status'))
			<div class="alert alert-success">
					{{ session('status') }}
			</div>
		@endif
		<h1>Bienvenid@ {{ auth()->user()->name }} </h1>
	@stop
