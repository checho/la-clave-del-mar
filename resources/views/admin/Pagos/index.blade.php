@extends('admin.layout')

@section('styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop

@section('header')
  <h1>
    Pagos
    <small>Lista de todos los pagos</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/administrador') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Pagos</li>
  </ol>
@stop

@section('content')
  <div class="box box-primary">
    <div class="box-header with-border">
      @include('admin.messages.info')
      <h3 class="box-title">Lista de pagos</h3>
      <a href="{{ route('pagos.create') }}" class="btn btn-primary pull-right">Generar nuevo pago <i class="fa fa-plus"></i></a>
    </div>
    <div class="box-body">
      <table id="pagos-table" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th with="20px">ID</th>
            <th>Nombre del pago</th>
            <th>Valor del pago</th>
            <th>Descripción del pago</th>
            <th>Fecha del pago</th>
            <th>Cuadre al que pertenece</th>
            <th>Accions</th>
          </tr>
        </thead>
        <tbody>
          @foreach($pagos as $pago)
            <tr>
              <td>{{ $pago->id }}</td>
              <td>{{ $pago->pago_nombre }}</td>
              <td>{{ $pago->pago_valor }}</td>
              @if(isset($pago->pago_desc))
                <td>{!! str_limit($pago->pago_desc, 30, '...') !!}</td>
              @else
                <td>No hay descripción</td>
              @endif
              <td>{{ $pago->created_at }}</td>
              <td><a href="{{ route('cuadres.show', $pago->cuadre->id) }}">{{ $pago->cuadre->created_at->toFormattedDateString() }}</a></td>

              <td class="pull-right">
                <a class="btn btn-xs btn-info" href="{{ route('pagos.show', $pago) }}" ><i class="fa fa-eye"></i></a>
                <a class="btn btn-xs btn-warning" href="{{ route('pagos.edit', $pago) }}" ><i class="fa fa-pencil-square-o"></i></a>
                <a class="btn btn-xs btn-danger" href="#" data-toggle="modal" data-target="#deletePago-{{ $pago->id }}"><i class="fa fa-trash"></i></a>
    					</td>

            </tr>

            @include('admin.Pagos.partials.deleteModal')

          @endforeach
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
@stop

@section('scripts')
  <!-- DataTables -->
  <script src="/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

  <!-- page script -->
  <script>
  $(function () {
    $('#pagos-table').DataTable({
      'paging'      : true,
      'searching'   : true,
      'ordering'    : true,
      'autoWidth'   : false,
      "scrollX"     : true,
    })
  })
</script>
@stop
