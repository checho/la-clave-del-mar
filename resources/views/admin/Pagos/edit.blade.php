@extends('admin.layout')

@section('styles')
  <!-- Select2 -->
  <link rel="stylesheet" href="/adminlte/bower_components/select2/dist/css/select2.min.css">
@stop

@section('header')
  <h1>
    Formulario de edición de pagos
    <small>formulario donde se editan pagos</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/administrador') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{ route('pagos.index') }}"><i class="fa fa-money"></i> Pagos</a></li>
    <li class="active">Editar pago</li>
  </ol>
@stop

@section('content')
  <div class="box box-success">
    <div class="box-header with-border">
      <h3 class="box-title">Editar pagos</h3>
      <a href="{{ URL::previous() }}" class="btn btn-info pull-right"><i class="fa fa-chevron-left"></i> Regresar</a>
    </div>
    <div class="box-body">
      @include('admin.messages.error')      
      <form role="form" action="{{ route('pagos.update',$pago) }}" method="post">
        @include('admin.Pagos.partials.editForm')
      </form>
    </div>
  </div>
@stop

@section('scripts')
  <!-- Select2 -->
  <script src="/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
  <!-- CK Editor -->
  <script src="/adminlte/bower_components/ckeditor/ckeditor.js"></script>

  <!-- Page script -->
  <script>
    $(function () {
      //Initialize Select2 Elements
      $('.select2-cuadre').select2({
        language: "es"
      })
      $('.select2-pago-nombre').select2({
        tags: true,
        language: "es",
        allowClear: true
      })
      $('.select2-pago-valor').select2({
        tags: true,
        language: "es",
        allowClear: true
      })
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor-pagos')
      //tooltips
      $('[data-toggle="tooltip"]').tooltip()
    })
  </script>

@stop
