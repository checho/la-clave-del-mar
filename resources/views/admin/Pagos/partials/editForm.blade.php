{{ csrf_field() }} {{ method_field('PUT') }}
<div class="row">
  <div class="form-group col-md-2">
    <label for="cuadre_fecha">Fecha del pago</label>
    <input class="form-control" type="text" value="{{ $pago->created_at }}" disabled>
  </div>
  <div class="form-group col-md-6 {{ $errors->has('cuadre_id') ? 'has-error' : '' }}">
    <label for="cuadre_id">Seleccione el cuadre <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="Seleccione la fecha del cuadre a la cual quiere añadir el pago"></i></label>
    <select class="form-control select2-cuadre" name="cuadre_id" data-placeholder="Seleccione el cuadre" style="width: 100%;">
      @foreach($cuadres as $cuadre)
        <option {{ $cuadre->id === $pago->cuadre_id ? 'selected' : '' }} value="{{ $cuadre->id }}">{{ $cuadre->created_at->toFormattedDateString() }}</option>
      @endforeach
    </select>
    @if ($errors->has('cuadre_id'))
    <label class="control-label" for="cuadre_id"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('cuadre_id') }}</strong>
    </label>
    @endif
  </div>
</div>
<div class="row">
  <div class="form-group col-md-6 {{ $errors->has('pago_nombre') ? 'has-error' : '' }}">
    <label for="pago_nombre">Pago</label>
    <select class="form-control select2-pago-nombre" name="pago_nombre" data-placeholder="Nombre del pago" style="width: 100%;">
      <option selected value="{{ $pago->pago_nombre }}">{{ $pago->pago_nombre }}</option>
    </select>
    @if ($errors->has('pago_nombre'))
    <label class="control-label" for="pago_nombre"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('pago_nombre') }}</strong>
    </label>
    @endif
  </div>
  <div class="form-group col-md-6 {{ $errors->has('pago_valor') ? 'has-error' : '' }}">
    <label for="pago_valor">valor pago</label>
    <select class="form-control select2-pago-valor" name="pago_valor" data-placeholder="Valor del pago" style="width: 100%;">
      <option selected value="{{ $pago->pago_valor }}">{{ $pago->pago_valor }}</option>
    </select>
    @if ($errors->has('pago_valor'))
    <label class="control-label" for="pago_valor"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('pago_valor') }}</strong>
    </label>
    @endif
  </div>
</div>
<div class="row">
  <div class="form-group col-md-12">
    <label for="pago_desc">Descripción del pago</label>
    <textarea id="editor-pagos" name="pago_desc" rows="10" cols="80">
      {{ old('pago_desc', $pago->pago_desc) }}
    </textarea>
  </div>
</div>
<button type="submit" class="btn btn-success">Terminar</button>
<a href="{{ URL::previous() }}" class="btn btn-info pull-right"><i class="fa fa-chevron-left"></i> Regresar</a>
