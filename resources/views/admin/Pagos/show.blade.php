@extends('admin.layout')

@section('styles')

@stop

@section('header')
  <h1>
    Detalle de pago
    <small>Detalle de un pago específico</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/administrador') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Pago - {{ $pago->created_at->toFormattedDateString() }}</li>
  </ol>
@stop

@section('content')
  <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Detalle del pago - {{ $pago->created_at->toFormattedDateString() }}</h3><span> Cuadre al que pertenece - <a href="{{ route('cuadres.show', $pago->cuadre->id) }}">{{ $pago->cuadre->created_at->toFormattedDateString() }}</a></span>
      <a href="{{ URL::previous() }}" class="btn btn-info pull-right"><i class="fa fa-chevron-left"></i> Regresar</a>
      <a style="margin-right: 10px"  href="{{ route('pagos.edit', $pago) }}" class="btn btn-primary pull-right"><i class="fa fa-pencil"></i> Editar pago</a>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <h3>Nombre del pago:</h3>
          <div class="col-md-11 offset-1">
            <strong>{{ $pago->pago_nombre }}</strong>
          </div>
        </div>
        <div class="col-md-6">
          <h3>Valor del pago:</h3>
          <div class="col-md-11 offset-1">
            <strong>{{ $pago->pago_valor }}</strong>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <h3>Descripción del pago</h3>
          <br>
          {!! $pago->pago_desc or 'No hay descripción' !!}
        </div>
      </div>
    </div>
  </div>
@stop

@section('scripts')

@stop
