@extends('admin.layout')

@section('styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop

@section('header')
  <h1>
    Usuarios
    <small>Lista de todos los usuarios</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/administrador') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Usuarios</li>
  </ol>
@stop

@section('content')
  <div class="box box-primary">
    <div class="box-header with-border">
      @include('admin.messages.info')
      @include('admin.messages.error')
      <h3 class="box-title">Lista de usuarios</h3>
      <a href="{{ route('users.create') }}" class="btn btn-primary pull-right">Crear nuevo Usuario <i class="fa fa-plus"></i></a>
    </div>
    <div class="box-body">
      <table id="cuadre-table" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th width="20px">ID</th>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Miembro desde</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach($users as $user)
            <tr>
              <td>{{ $user->id }}</td>
              <td>{{ $user->name }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->created_at->toFormattedDateString() }}</td>

              <td class="pull-right">
                <a class="btn btn-xs btn-info" href="{{ route('users.show', $user) }}" ><i class="fa fa-eye"></i></a>
                <a class="btn btn-xs btn-warning" href="{{ route('users.edit', $user) }}" ><i class="fa fa-pencil-square-o"></i></a>
                <a class="btn btn-xs btn-danger" href="#" data-toggle="modal" data-target="#deleteUser-{{ $user->id }}"><i class="fa fa-trash"></i></a>
    					</td>
            </tr>

            @include('admin.Users.partials.deleteModal')

          @endforeach
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
@stop

@section('scripts')
  <!-- DataTables -->
  <script src="/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

  <!-- page script -->
  <script>
  $(function () {
    $('#cuadre-table').DataTable({
      'paging'      : true,
      'searching'   : true,
      'ordering'    : true,
      'autoWidth'   : false,
      "scrollX"     : true,
    })
  })
</script>
@stop
