{{ csrf_field() }}
<div class="row">
  <div class="form-group col-md-6 {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name">Nombre del usuario</label>
    <input class="form-control" type="text" name="name" value="{{ old('name') }}">
    @if ($errors->has('name'))
    <label class="control-label" for="name"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('name') }}</strong>
    </label>
    @endif
  </div>
  <div class="form-group col-md-6 {{ $errors->has('email') ? 'has-error' : '' }}">
    <label for="Email">Email del usuario</label>
    <input class="form-control" type="email" name="email" value="{{ old('email') }}">
    @if ($errors->has('email'))
    <label class="control-label" for="email"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('email') }}</strong>
    </label>
    @endif
  </div>
</div>
<div class="row">
  <div class="form-group col-md-4 {{ $errors->has('password') ? 'has-error' : '' }}">
    <label for="password">Contraseña</label>
    <input class="form-control" type="password" name="password">
    @if ($errors->has('password'))
    <label class="control-label" for="password"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('password') }}</strong>
    </label>
    @endif
  </div>
</div>
<div class="row">
  <div class="form-group col-md-4 {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
    <label for="password_confirmation">Confirmar contraseña</label>
    <input class="form-control" type="password" name="password_confirmation">
    @if ($errors->has('password_confirmation'))
    <label class="control-label" for="password_confirmation"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('password_confirmation') }}</strong>
    </label>
    @endif
  </div>
</div>

<button type="submit" class="btn btn-success">Terminar</button>
<a href="{{ URL::previous() }}" class="btn btn-info pull-right"><i class="fa fa-chevron-left"></i> Regresar</a>
