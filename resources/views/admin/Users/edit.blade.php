@extends('admin.layout')

@section('styles')

@stop

@section('header')
  <h1>
    Formulario de edición de usuario
    <small>formulario donde se edita un usuario</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/administrador') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{ route('users.index') }}"><i class="fa fa-user"></i> Usuarios</a></li>
    <li class="active">Editar usuario</li>
  </ol>
@stop

@section('content')
  <div class="box box-success">
    <div class="box-header with-border">
      <h3 class="box-title">Editar usuario</h3>
      <a href="{{ URL::previous() }}" class="btn btn-info pull-right"><i class="fa fa-chevron-left"></i> Regresar</a>
    </div>
    <div class="box-body">
      @include('admin.messages.error')      
      <form role="form" action="{{ route('users.update', $user) }}" method="post">
        @include('admin.Users.partials.editForm')
      </form>
    </div>
  </div>
@stop

@section('scripts')

@stop
