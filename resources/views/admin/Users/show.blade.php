@extends('admin.layout')

@section('styles')

@stop

@section('header')
  <h1>
    Detalle de usuario
    <small>Detalle de un usuario específico</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/administrador') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Usuario - {{ $user->name }}</li>
  </ol>
@stop

@section('content')
  <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Detalle del usuario - {{ $user->name }}</h3>
      <a href="{{ URL::previous() }}" class="btn btn-info pull-right"><i class="fa fa-chevron-left"></i> Regresar</a>
      <a style="margin-right: 10px"  href="{{ route('users.edit', $user) }}" class="btn btn-primary pull-right"><i class="fa fa-pencil"></i> Editar usuario</a>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <h3>Email:</h3>
          <div class="col-md-11 offset-1">
            <strong>{{ $user->email }}</strong>
          </div>
        </div>
        <div class="col-md-6">
          <h3>Miembro desde:</h3>
          <div class="col-md-11 offset-1">
            <strong>{{ $user->created_at->toFormattedDateString() }}</strong>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop

@section('scripts')

@stop
