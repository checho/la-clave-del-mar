@extends('admin.layout')

@section('styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@stop

@section('header')
  <h1>
    Cuadres Diarios
    <small>Resumen de todos los cuadres día por día</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/administrador') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Cuadres</li>
  </ol>
@stop

@section('content')
  <div class="box box-primary">
    <div class="box-header with-border">
      @include('admin.messages.info')
      <h3 class="box-title">Lista de cuadres</h3>
      <a href="{{ route('cuadres.create') }}" class="btn btn-primary pull-right">Generar nuevo cuadre <i class="fa fa-plus"></i></a>
    </div>
    <div class="box-body">
      <table id="cuadre-table" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th width="20px">ID</th>
            <th>Fecha cuadre</th>
            <th>Venta</th>
            <th>Propinas</th>
            <th class="text-center">Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach($cuadres as $cuadre)
            <tr>
              <td>{{ $cuadre->id }}</td>
              <td>{{ $cuadre->created_at->toFormattedDateString() }}</td>
              <td>{{ $cuadre->cuadre_venta }}</td>
              <td>{!! $cuadre->cuadre_propina !!}</td>

              <td class="pull-right">
                <a class="btn btn-xs btn-info" href="{{ route('cuadres.show', $cuadre) }}" ><i class="fa fa-eye"></i></a>
                <a class="btn btn-xs btn-warning" href="{{ route('cuadres.edit', $cuadre) }}" ><i class="fa fa-pencil-square-o"></i></a>
                <a class="btn btn-xs btn-danger" href="#" data-toggle="modal" data-target="#deleteCuadre-{{ $cuadre->id }}"><i class="fa fa-trash"></i></a>
    					</td>
            </tr>

            @include('admin.Cuadres.partials.deleteModal')

          @endforeach
        </tbody>
      </table>
    </div>
    <div class="box-footer">
      <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
@stop

@section('scripts')
  <!-- DataTables -->
  <script src="/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

  <!-- page script -->
  <script>
  $(function () {
    $('#cuadre-table').DataTable({
      'paging'      : true,
      'searching'   : true,
      'ordering'    : true,
      'autoWidth'   : false,
      "scrollX"     : true,
    })
  })
</script>
@stop
