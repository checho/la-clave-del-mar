{{ csrf_field() }} {{ method_field('PUT') }}
<div class="row">
  <div class="form-group col-md-2">
    <label for="cuadre_fecha">Fecha de cuadre</label>
    <input class="form-control" type="text" value="{{ $cuadre->created_at }}" disabled>
  </div>
  <div class="form-group col-md-5 {{ $errors->has('cuadre_venta') ? 'has-error' : '' }}">
    <label for="cuadre_venta">Venta</label>
    <input class="form-control" type="number" name="cuadre_venta" value="{{ old('cuadre_venta', $cuadre->cuadre_venta ) }}" min="0" max="10000000">
    @if ($errors->has('cuadre_venta'))
    <label class="control-label" for="cuadre_venta"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('cuadre_venta') }}</strong>
    </label>
    @endif
  </div>
</div>
<div class="row">
  <div class="form-group col-md-4 {{ $errors->has('cuadre_base') ? 'has-error' : '' }}">
    <label for="cuadre_base">Base</label>
    <input class="form-control" type="number" name="cuadre_base" value="{{ old('cuadre_base', $cuadre->cuadre_base ) }}" min="0" max="1000000">
    @if ($errors->has('cuadre_base'))
    <label class="control-label" for="cuadre_base"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('cuadre_base') }}</strong>
    </label>
    @endif
  </div>
  <div class="form-group col-md-4 {{ $errors->has('cuadre_propina') ? 'has-error' : '' }}">
    <label for="cuadre_propina">Propinas</label>
    <input class="form-control" type="number" name="cuadre_propina" value="{{ old('cuadre_propina', $cuadre->cuadre_propina) }}" min="0" max="500000">
    @if ($errors->has('cuadre_propina'))
    <label class="control-label" for="cuadre_propina"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('cuadre_propina') }}</strong>
    </label>
    @endif
  </div>
</div>
<div class="row">
  <div class="form-group col-md-6 {{ $errors->has('pagos') ? 'has-error' : '' }}">
    <label for="pagos">Pago / valor pago <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="Ej: andres-50000 luis-78500"></i></label>
    <select class="form-control select2-pagos" multiple="multiple" name="pagos[]" data-placeholder="Dele un nombre al pago y su valor, separelos por '-'" style="width: 100%;">
      @foreach($cuadre->pagos as $pago)
        <option selected value="{{ $pago->pago_nombre.'-'.$pago->pago_valor }}">{{ $pago->pago_nombre.'-'.$pago->pago_valor }}</option>
      @endforeach
    </select>
    @if ($errors->has('pagos'))
    <label class="control-label" for="pagos"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('pagos') }}</strong>
    </label>
    @endif
  </div>
  <div class="form-group col-md-6 {{ $errors->has('datafonos') ? 'has-error' : '' }}">
    <label for="datafonos">Datáfono / Valor datáfono <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="Ej: visa-50000 AMEX-78500"></i></label>
    <select class="form-control select2-datafonos" multiple="multiple" name="datafonos[]" data-placeholder="Escriba el tipo de datáfono y su valor, separelos por '-'" style="width: 100%;">
      @foreach($cuadre->datafonos as $datafono)
        <option selected value="{{ $datafono->datafono_tipo.'-'.$datafono->datafono_valor }}">{{ $datafono->datafono_tipo.'-'.$datafono->datafono_valor }}</option>
      @endforeach

    </select>
    @if ($errors->has('datafonos'))
    <label class="control-label" for="datafonos"><i class="fa fa-times-circle-o"></i>
      <strong>{{ $errors->first('datafonos') }}</strong>
    </label>
    @endif
  </div>
</div>
<div class="row">
  <div class="form-group col-md-12">
    <label for="cuadre_descripcion">Descripción del cuadre</label>
    <textarea id="editor-cuadres" name="cuadre_descripcion"  rows="10" cols="80">
      {{ old('cuadre_descripcion', $cuadre->cuadre_descripcion ) }}
    </textarea>
  </div>
</div>

<button type="submit" class="btn btn-success">Terminar</button>
<a href="{{ URL::previous() }}" class="btn btn-info pull-right"><i class="fa fa-chevron-left"></i> Regresar</a>
