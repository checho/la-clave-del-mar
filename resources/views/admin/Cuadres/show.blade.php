@extends('admin.layout')

@section('styles')

@stop

@section('header')
  <h1>
    Detalle de cuadre
    <small>Detalle de un cuandre específico</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/administrador') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Cuadre - {{ $cuadre->created_at->toFormattedDateString() }}</li>
  </ol>
@stop

@section('content')
  <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Detalle del cuadre - {{ $cuadre->created_at->toFormattedDateString() }}</h3>
      <a href="{{ URL::previous() }}" class="btn btn-info pull-right"><i class="fa fa-chevron-left"></i> Regresar</a>
      <a style="margin-right: 10px"  href="{{ route('cuadres.edit', $cuadre) }}" class="btn btn-primary pull-right"><i class="fa fa-pencil"></i> Editar cuadre</a>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <h3>Ventas:</h3>
          <div class="col-md-11 offset-1">
            <strong class="label label-primary">{{ number_format($cuadre->cuadre_venta,0,'','.') }} <i class="fa fa-dollar"></i></strong>
          </div>
        </div>
        <div class="col-md-6">
          <h3>Base:</h3>
          <div class="col-md-11 offset-1">
            <strong class="label label-primary">{{ number_format($cuadre->cuadre_base,0,'','.') }} <i class="fa fa-dollar"></i></strong>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <h3>Propinas:</h3>
          <div class="col-md-11 offset-1">
            <strong class="label label-primary">{{ number_format($cuadre->cuadre_propina,0,'','.') }} <i class="fa fa-dollar"></i></strong>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-6">
          <h3>Pagos:</h3>
            @forelse($cuadre->pagos as $pago)
              <div class="col-md-11 offset-1">
                <span><strong>Pago nombre: </strong></span><span><a href="{{ route('pagos.show', $pago) }}">{{ $pago->pago_nombre }}</a></span><br>
                <span><strong>Pago valor: </strong></span><span class="label label-primary">{{ number_format($pago->pago_valor,0,'','.') }} <i class="fa fa-dollar"></i></span><br>
                  @if(isset($pago->pago_desc))
                    <span><strong>Pago descripción: </strong></span><span>{!! str_limit($pago->pago_desc, 50, '...') !!}</span>
                  @else
                    <span><strong>Pago descripción: </strong></span><span>No hay descripción</span>
                  @endif
                @unless($loop->last)
                  <hr>
                @endunless
              </div>
            @empty
              <div class="col-md-12 offset-1">
                <strong>No se registraron pagos para este cuadre. Si quieres registrar un nuevo pago has clic <a href="{{ route('pagos.create') }}">aquí</a> </strong>
              </div>
            @endforelse
            @if($cuadre->sumaPagos() > 0)
              <div class="col-md-10 offset-1">
                <h3>Total pagos: </h3><br>
                <strong class="alert alert-success">{{ number_format($cuadre->sumaPagos(),0,'','.') }} <i class="fa fa-dollar"></i></strong>
              </div>
            @endif
        </div>
        <div class="col-md-6">
          <h3>Datafonos:</h3>
            @forelse($cuadre->datafonos as $datafono)
              <div class="col-md-11 offset-1">
                <span><strong>Tipo de datáfono: </strong></span><span><a href="{{ route('datafonos.show', $datafono) }}">{{ $datafono->datafono_tipo }}</a></span><br>
                <span><strong>Datáfono valor: </strong></span><span class="label label-primary">{{ number_format($datafono->datafono_valor,0,'','.') }} <i class="fa fa-dollar"></i></span><br>
                  @if(isset($datafono->datafono_desc))
                    <span><strong>Datáfono descripción: </strong></span><span>{!! str_limit($datafono->datafono_desc, 50, '...') !!}</span>
                  @else
                    <span><strong>Datáfono descripción: </strong></span><span>No hay descripción</span>
                  @endif
                @unless($loop->last)
                  <hr>
                @endunless
              </div>
            @empty
              <div class="col-md-12 offset-1">
                <strong>No se registraron datáfonos para este cuadre. Si quieres registrar un nuevo datáfono has clic <a href="{{ route('datafonos.create') }}">aquí</a> </strong>
              </div>
            @endforelse
            @if($cuadre->sumaDatafonos() > 0)
              <div class="col-md-10 offset-1">
                <h3>Total datáfonos: </h3><br>
                <strong class="alert alert-success">{{ number_format($cuadre->sumaDatafonos(),0,'','.') }} <i class="fa fa-dollar"></i></strong>
              </div>
            @endif
        </div>
      </div>
      <br>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <h3>Descripción:</h3>
          {!! $cuadre->cuadre_descripcion or 'No hay descripción' !!}
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-6">
          <h3>Total venta:</h3>
          <div class="col-md-11 offset-1">
            <h5 class="alert alert-success">{{ number_format($cuadre->totalBase(),0,'','.') }} <i class="fa fa-dollar"></i></h5>
          </div>
        </div>
        <div class="col-md-6">
          <h3>Salida caja y datáfono:</h3>
          <div class="col-md-11 offset-1">
            <h5 class="alert alert-success">{{ number_format($cuadre->totalSumaPD(),0,'','.') }} <i class="fa fa-dollar"></i></h5>
          </div>
        </div>
        <div class="col-md-12">
          <h3>Tener en efectivo:</h3>
          <div class="col-md-11 offset-1 alert alert-success">
            <h3>{{ number_format($cuadre->totalFinal(),0,'','.') }} <i class="fa fa-dollar"></i></h3>
          </div>
        </div>
      </div>

    </div>
  </div>
@stop

@section('scripts')

@stop
