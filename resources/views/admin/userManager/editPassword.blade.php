@extends('admin.layout')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body box-profile">
        @include('admin.messages.error')
        <form action="{{ route('edit.data.profile.password.form', auth()->user()) }}" enctype="multipart/form-data" method="post">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <hr>
          <center>
            <div class="form-group has-feedback {{ $errors->has('current_password') ? 'has-error' : '' }}">
              <input id="current_password" class="profile-username text-center form-control" type="password" name="current_password" placeholder="Contraseña actual">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              @if ($errors->has('current_password'))
              <label class="control-label" for="current_password"><i class="fa fa-times-circle-o"></i>
                <strong>{{ $errors->first('current_password') }}</strong>
              </label>
              @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
              <input id="password" class="profile-username text-center form-control" type="password" name="password" placeholder="Nueva contraseña">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              @if ($errors->has('password'))
              <label class="control-label" for="password"><i class="fa fa-times-circle-o"></i>
                <strong>{{ $errors->first('password') }}</strong>
              </label>
              @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
              <input id="password_confirmation" class="profile-username text-center form-control" type="password" name="password_confirmation" placeholder="Repita su nueva contraseña">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              @if ($errors->has('password_confirmation'))
              <label class="control-label" for="password_confirmation"><i class="fa fa-times-circle-o"></i>
                <strong>{{ $errors->first('password_confirmation') }}</strong>
              </label>
              @endif
            </div>
          </center>
          <hr>
          <button type="submit" class="btn btn-primary btn-block"><b>Cambiar contraseña</b></button>
          <a href="{{ URL::previous() }}" class="btn btn-primary btn-block"><b><i class="fa fa-chevron-left"></i> Regresar</b></a>
        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
@stop
