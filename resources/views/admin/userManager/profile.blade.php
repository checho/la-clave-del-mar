@extends('admin.layout')

@section('content')
<div class="row">
  <div class="col-md-12">
    @include('admin.messages.info')
    @include('admin.messages.error')
    <div class="box box-primary">
      <div class="box-body box-profile">        
        <img class="profile-user-img img-responsive img-circle" src="{{ asset('uploads/'.auth()->user()->image) }}" alt="User profile picture">
        <h3 class="profile-username text-center">{{ auth()->user()->name }}</h3>
        <p class="text-muted text-center">{{ auth()->user()->email }}</p>
        <ul class="list-group list-group-unbordered">
          {{-- <li class="list-group-item">
            <b>Followers</b> <a class="pull-right">1,322</a>
          </li>
          <li class="list-group-item">
            <b>Following</b> <a class="pull-right">543</a>
          </li>
          <li class="list-group-item">
            <b>Friends</b> <a class="pull-right">13,287</a>
          </li> --}}
        </ul>
        <a href="{{ route('show.edit.profile.form') }}" class="btn btn-primary btn-block"><b>Editar perfil</b></a>
        <a href="{{ route('show.edit.profile.password.form') }}" class="btn btn-primary btn-block"><b>Cambiar contraseña</b></a>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
@stop
