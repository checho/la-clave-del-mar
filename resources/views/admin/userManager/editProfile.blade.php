@extends('admin.layout')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body box-profile">
        @include('admin.messages.error')
        <form action="{{ route('edit.data.profile.form', auth()->user()) }}" enctype="multipart/form-data" method="post">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <img class="profile-user-img img-responsive img-circle" src="{{ asset('uploads/'.$user->image) }}" alt="{{ auth()->user()->name }}">
          <hr>
          <center>
            <div class="form-group has-feedback {{ $errors->has('image') ? 'has-error' : '' }}">
              <input class="form-control" type="file" name="image">
              @if ($errors->has('image'))
              <label class="control-label" for="image"><i class="fa fa-times-circle-o"></i>
                <strong>{{ $errors->first('image') }}</strong>
              </label>
              @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
              <input class="profile-username text-center form-control" type="text" name="name" placeholder="Nombre" value="{{ old('name' , auth()->user()->name ) }}">
              @if ($errors->has('name'))
              <label class="control-label" for="name"><i class="fa fa-times-circle-o"></i>
                <strong>{{ $errors->first('name') }}</strong>
              </label>
              @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
              <input class="profile-username text-center form-control" type="email" name="email" placeholder="Correo electronico" value="{{ old('email' , auth()->user()->email ) }}">
              @if ($errors->has('email'))
              <label class="control-label" for="email"><i class="fa fa-times-circle-o"></i>
                <strong>{{ $errors->first('email') }}</strong>
              </label>
              @endif
            </div>
          </center>
          <hr>
          <button type="submit" class="btn btn-primary btn-block"><b>Editar perfil</b></button>
          <a href="{{ URL::previous() }}" class="btn btn-primary btn-block"><b><i class="fa fa-chevron-left"></i> Regresar</b></a>
        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
@stop
