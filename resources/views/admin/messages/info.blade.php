@if(Session::has('info'))	
	<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> Info!</h4>
		{{ Session::get('info') }}
	</div>
@endif
