@if(Session::has('error'))
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert">
			<span>&times;</span>
		</button>
		<center><h3>{{ Session::get('error') }}</h3></center>
		<br>
		<center><i class="fa fa-frown-o fa-5x" aria-hidden="true"></i></center>
	</div>
@endif
