<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatafonosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datafonos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('datafono_tipo');
            $table->string('datafono_valor');
            $table->text('datafono_desc')->nullable();
            $table->integer('cuadre_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datafonos');
    }
}
