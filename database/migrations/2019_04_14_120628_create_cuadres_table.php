<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuadresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuadres', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cuadre_venta');
            $table->unsignedInteger('cuadre_base');
            $table->unsignedInteger('cuadre_propina');
            $table->text('cuadre_descripcion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuadres');
    }
}
