<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/administrador', function() {
  return view('admin.dashboard');
})->middleware('auth');

Route::group([
  'prefix' => 'admin',
  'middleware' => 'auth'],
function(){
  Route::get('profile', 'userManagerController@profile')->name('profile');
  Route::get('profile/edit', 'userManagerController@showEditProfile')->name('show.edit.profile.form');
  Route::put('profile/edit/{user}', 'userManagerController@editProfile')->name('edit.data.profile.form');
  Route::get('profile/edit/password', 'userManagerController@showEditPassword')->name('show.edit.profile.password.form');
  Route::put('profile/edit/password/{user}', 'userManagerController@editPassword')->name('edit.data.profile.password.form');
  Route::resource('users', 'UserController');
  Route::resource('cuadres', 'CuadreController');
  Route::resource('pagos', 'PagoController');
  Route::resource('datafonos', 'DatafonoController');
});
